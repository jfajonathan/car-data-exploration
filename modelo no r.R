dados <- read.csv('dados/car data.csv')
library(tidyverse)

glimpse(dados)
unique(dados$Car_Name) %>% length()
mod = glm(Selling_Price ~ .,
    data = dados %>% select(-Car_Name),
    family = Gamma(link = "log"))

plot(mod)

cor(dados$Selling_Price,predict(mod,type = "response"))^2
  
dados_pad <- dados %>% select(-Selling_Price) %>%  mutate_if(is.numeric,.funs = scale)
dados_pad$Selling_Price = dados$Selling_Price
mod_pad = glm(Selling_Price ~ .,
          data = dados_pad %>% select(-Car_Name),
          family = Gamma(link = "log"))
summary(mod_pad)
cor(dados$Selling_Price,predict(mod_pad,type = "response"))^2
